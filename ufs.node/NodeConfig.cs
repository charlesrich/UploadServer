﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ufs.node
{
    /*
     * "mimeModifiers": [
      {
        "ext": "",
        "type": "",
        "opt": "add"
      },
     */

    public class MimeModifer
    {
        /// <summary>
        /// 后缀名
        /// </summary>
        public string Ext { get; set; }

        /// <summary>
        /// ContentType
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 操作，add或remove
        /// </summary>
        public MimeModiferOpts Opt { get; set; }
    }

    public enum MimeModiferOpts
    {
        add = 1,
        remove = 2
    }


    public class NodeConfig
    {
        /// <summary>
        /// mime修改器集合
        /// </summary>
        public MimeModifer[] MimeModifiers { get; set; }


        /// <summary>
        /// 域名
        /// </summary>
        /// <value>The domain.</value>
        public string Domain { get; set; }

        /// <summary>
        /// 文件根目录
        /// </summary>
        /// <value>The root path.</value>
        public string PhysicalPath { get; set; }

        /// <summary>
        /// 请求根目录
        /// </summary>
        /// <value>The request path.</value>
        public string VirtualPath { get; set; }

        /// <summary>
        /// 缓存秒数
        /// </summary>
        /// <value>The cache period.</value>
        public int CachePeriod { get; set; }

        /// <summary>
        /// 是否启用缩略图
        /// </summary>
        public  bool EnableThumbnail { get; set; }
        /// <summary>
        /// 缩略图后缀
        /// </summary>
        public HashSet<string> ThumbnailExts { get; set; }


        /// <summary>
        /// 允许上传的IP列表
        /// </summary>
        public HashSet<string> AllowIPs { get; set; }
    }
}