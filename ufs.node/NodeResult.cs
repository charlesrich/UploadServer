﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ufs.node
{
    public class NodeResult
    {
        public NodeResult()
        {
        }

        public string FileUrl { get; set; }
        public bool Success { get; set; }
        public string Msg { get; set; }

        public override string ToString()
        {
            return "{\"success\":" + Success.ToString().ToLower() + ",\"msg\":\"" + Msg + "\",\"fileUrl\":\"" + FileUrl + "\"}";
        }
    }
}
